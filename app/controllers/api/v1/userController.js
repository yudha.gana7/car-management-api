const userService = require("../../../services/userService")
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../../../models");
const SALT = 10;

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, SALT, (err, encryptedPassword) => {
            if (!!err) {
                reject(err);
                return;
            }

            resolve(encryptedPassword);
        });
    });
}

function checkPassword(encryptedPassword, password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
            if (!!err) {
                reject(err);
                return;
            }

            resolve(isPasswordCorrect);
        });
    });
}

function createToken(payload) {
    return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia");
}

module.exports = {
    async register(req, res) {
        const name = req.body.name;
        const email = req.body.email;
        const password = await encryptPassword(req.body.password);
        const role = "member";
        const user = await userService.create({
            name,
            email,
            password,
            role
        });
        res.status(201).json({
            id: user.id,
            name: user.name,
            email: user.email,
            password: user.password,
            role: user.role,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        });
    },

    async login(req, res) {
        const email = req.body.email.toLowerCase(); // Biar case insensitive
        const password = req.body.password;

        const user = await userService.findOne({
            where: { email },
        });

        if (!user) {
            res.status(404).json({ message: "Email Not Found" });
            return;
        }

        const isPasswordCorrect = await checkPassword(
            user.password,
            password
        );

        if (!isPasswordCorrect) {
            res.status(401).json({ message: "Password Incorrect!" });
            return;
        }

        const token = createToken({
            id: user.id,
            name: user.name,
            email: user.email,
            role: user.role,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        });

        res.status(201).json({
            id: user.id,
            name: user.name,
            email: user.email,
            role: user.role,
            token,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        });
    },

    async whoAmI(req, res) {
        res.status(200).json(req.user);
    },

    async authorize(req, res, next) {
        try {
            const token = req.headers.authorization;
            // const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(
                token,
                process.env.JWT_SIGNATURE_KEY || "Top Secret"
            );

            req.user = await userService.findByPk(tokenPayload.id);
            next();
        } catch (err) {
            console.error(err);
            res.status(401).json({
                message: "Unauthorized",
            });
        }
    },

    async isAdminOrSuperAdmin(req, res, next) {
        if (!(req.user.role === "superadmin" || req.user.role === "admin")) {
            res.json({
                message: "You are not superadmin or admin, therefore you're not allowed to continue"
            });
            return;
        }
        next();
    },

    async isSuperAdmin(req, res, next) {
        if (!(req.user.role === "superadmin")) {
            res.json({
                message: "You're not superadmin, therefore you're not allowed to change anything",
            });
            return;
        }
        next();
    },

    async getUsers(req, res) {
        try {
            const users = await userService.getUsers();
            res.status(200).json({
                status: "Success Show All User Data",
                data: {
                    users
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to get All Users",
                errors: [err.message]
            })
        }
    },

    async createUsers(req, res) {
        try {
            const users = await userService.create(req.body);
            res.status(201).json({
                status: "Data have created successfully",
                data: {
                    users
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to create users",
                errors: [err.message]
            })
        }
    },

    async updateUsers(req, res) {
        try {
            const users = await userService.update(req.params.id, req.body);
            res.status(200).json({
                status: "Successfully updated member data",
                data: {
                    users
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to update users",
                errors: [err.message]
            })
        }
    },

    async show(req, res) {
        try {
            const users = await userService.findByPk(req.params.id);
            res.status(200).json({
                status: "Success Show Users Data",
                data: {
                    users
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to show users data",
                errors: [err.message]
            })
        }
    },

    async destroyUsers(req, res) {
        try {
            const users = await userService.delete(req.params.id);
            res.status(200).json({
                status: "Data Successfully Deleted From Database",
                data: {
                    users
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to delete data",
                errors: [err.message]
            })
        }
    },
};