const carService = require("../../../services/carService")

module.exports = {
    async list(req, res) {
        try {
            const cars = await carService.list();
            res.status(200).json({
                status: "Success Showing List Of Cars",
                data: {
                    cars
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed To Showing List",
                errors: [err.message]
            })
        }
    },

    async create(req, res) {
        try {
            req.body.createdBy = req.user.name;
            const cars = await carService.create(req.body, req.body.createdBy);
            // const cars = await carService.create(req.body);
            console.log(req.body)
            res.status(201).json({
                status: "Success Create New Cars",
                data: {
                    cars
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to create new Cars",
                errors: [err.message]
            })
        }
    },

    async update(req, res) {
        try {
            req.body.updatedBy = req.user.name;
            const cars = await carService.update(req.params.id, req.body, req.body.updatedBy);
            // const cars = await carService.update(req.params.id, req.body);
            res.status(200).json({
                status: "Cars Successfully Updated",
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to Update Cars",
                errors: [err.message]
            })
        }
    },

    async show(req, res) {
        try {
            const cars = await carService.get(req.params.id);
            res.status(200).json({
                status: "Show Car Data By Id",
                data: {
                    cars
                }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to Show Car Data",
                errors: [err.message]
            })
        }
    },

    async delete(req, res) {
        try {
            const cars = await carService.update(req.params.id, { deletedBy: req.user.name });
            res.status(200).json({
                status: "Data have deleted successfully",
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed",
                errors: [err.message]
            })
        }
    },

    async destroy(req, res) {
        try {
            const cars = await carService.delete(req.params.id);
            res.status(200).json({
                status: "Cars Successfully Deleted",
                // data: {
                //   cars
                // }
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed to Delete Cars",
                errors: [err.message]
            })
        }
    },

}