## Binar Car Management API

Repository ini berisi code pada Binar Car Management API pada challenge chapter 6 pada Binar Academy. Pada Code ini berisi 2 buah  subcode yang akan mengatur aktivitas Create, Read, Update, dan Delete pada table User dan juga table Cars. Untuk kebutuhan authorisasi disediakan 3 buah role yaitu superadmin, admin, dan member. dibutuhkan role superadmin dan admin untuk melakukan fitur akses data mobil dan juga user.



```sh
npm install
```

## Script untuk mengatur database

terdapat beberapa code yang akan mengatur sistem database

- `npm db:create` berfungsi untuk membuat database berdasarkan nama data yang telah diatur
- `npm db:drop` berfungsi untuk menghapus database yang telah dibuat sebelumnya
- `npm db:migrate` berfungsi untuk menjalankan database migration berdasarkan migration yang telah dibuat
- `npm db:seed` berfungsi untuk melakukan seeding/ input data kedalam database
- `npm db:rollback` berfungsi untuk membatalkan migrasi terakhir atau kembali ke migrasi sebelumnya

